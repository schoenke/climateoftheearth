# Projects for Climate of The Earth WS 23/24

1. Run the ocean model MOM5 3 nm (1850) 1949-2008 driven by reconstructed atmospheric and hydrological data, sensitivity experiments with constant river runoff (Meier and Kauker 2003, doi:10.1029/2003JC001799) What causes the multi-decadal variations in Baltic Sea salinity? (Cyril, Markus)
2. Investigate changing marine heat waves (Hobday et al. 2018, https://www.jstor.org/stable/26542662) Have the intensity and frequency of MHW increased? (Cyril, Markus)  
3. Investigate changing sea level extremes (Weisse et al. 2021, https://doi.org/10.5194/esd-12-871-2021) Have sea level extremes increased? Are there spatial differences in the changes? (Sven, Markus) 
4. Investigate changing saltwater inflows and vertical stratification in salinity (Mohrholz 2018, https://doi.org/10.3389/fmars.2018.00384)How did saltwater inflows affect the vertical stratification? (Florian,Markus)
5. Investigate changing horizontal circulation and overturning circulation (Placke et al. 2021, https://doi.org/10.1029/2020JC016079) Did the large scale ocean circulation change with time? (Sven, Markus) 
6. Investigate changing climate variability in the Baltic Sea area (Lehmann et al. 2011, doi: 10.3354/cr00876) How did natural variability affect the climate in the Baltic Sea region, e.g. sea ice cover (Florian, Markus)
7. Investigate changing precipitation extremes in the Baltic Sea region(Cardell et al. 2020, https://doi.org/10.1002/joc.6490). Did precipitation extremes increase with increasing air temperature? (Cyril, Markus) 