# Climate of the Earth

In this class, you will get an introduction to fundamental processes of the climate system. On this website, you will find interactive tutorials where we step by step build our own climate model. In the end, we will use a comprehensive coupled ice-ocean model simulating the Baltic Sea.

## Information about the Course

- Lectures at Rostock University (Master in Physics)
- 6 ECTS
- Teachers: Markus Meier, Florian Börgel, Sven Karsten, Hagen Radtke


```{figure} figures/euro_cordex_dalle.png
---
width: 100%
name: euro-cordex-climate-simulation
---
EURO-CORDEX climate simulation visualization using Dalle-3
```

## Lecture Content

- Fundamental processes of the climate system (greenhouse effect, radiation balance, climate sensitivity, stability, and feedbacks)
- Basic methods of the analysis and modeling of the climate system with a focus on the ocean
- Equations of motion of the large-scale circulation with a focus on the ocean
- Coupled atmosphere–ocean–sea-ice models
- Spatial and temporal variability of the climate system
- Anthropogenic climate change and natural climate variability (externally and internally driven climate variability)


## Literature

### Climate Assessment

- IPCC ([www.ipcc.ch](https://www.ipcc.ch), open access)
- BACC I and II ([www.baltic.earth](https://www.baltic.earth), open access)
- NOSCCA ([Springer Link](https://link.springer.com/book/10.1007/978-3-319-39745-0), open access)

### Atmospheric and Oceanic Fluid Dynamics

- James R. Holton: An Introduction to Dynamic Meteorology
- Geoffrey K. Vallis: Climate and the Oceans
- Geoffrey K. Vallis: Atmospheric and Oceanic Fluid Dynamics
- Olbers, Willebrand, and Eden: Ocean Dynamics, Springer (2012)

### Lecture Notes

- Courtesy: Lectures from Ulrich Cubasch, Erik Kjellström
- Lecture notes by Dietmar Dommenget: [Climate Dynamics Notes](http://users.monash.edu.au/~dietmard/teaching/dommenget.climate.dynamics.notes.pdf)
- Hamburger Bildungsserver: [Klimawandel](http://bildungsserver.hamburg.de/klimawandel/) (only in German)
